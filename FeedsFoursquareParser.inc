<?php

/**
 * @file
 * Feeds parser class for Fourquare
 */

/**
 * Class definition for Foursquare Parser.
 *
 * Parses data from Foursquare API
 */
class FeedsFoursquareParser extends FeedsParser {

  /**
   * Parse the extra mapping sources provided by this parser.
   *
   * @param $fetcher_result FeedsFetcherResult
   * @param $source FeedsSource
   *
   * @see FeedsParser::parse()
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = new FeedsParserResult();  

    $response_json = $fetcher_result->getRaw();
    if (isset($response_json)) {
      $response_data = json_decode($response_json);
      if (isset($response_json)) {
        $venue = $response_data->response->venue;
        $item = array(
          'venue.id' => isset($venue->id) ? $venue->id : '',
          'venue.name' => isset($venue->name) ? $venue->name : '',
          'venue.contact.formattedPhone' => isset($venue->contact->formattedPhone) ? $venue->contact->formattedPhone : '',
          'venue.contact.twitter' => isset($venue->contact->twitter) ? $venue->contact->twitter : '',
          'venue.location.address' => isset($venue->location->address) ? $venue->location->address : '',
          'venue.location.lat' => isset($venue->location->lat) ? $venue->location->lat : '',
          'venue.location.lng' => isset($venue->location->lng) ? $venue->location->lng : '',
          'venue.location.postalCode' => isset($venue->location->postalCode) ? $venue->location->postalCode : '',
          'venue.location.city' => isset($venue->location->city) ? $venue->location->city : '',
          'venue.location.state' => isset($venue->location->state) ? $venue->location->state : '',
          'venue.location.country' => isset($venue->location->country) ? $venue->location->country : '',
          'venue.stats.checkinsCount' => isset($venue->stats->checkinsCount) ? $venue->stats->checkinsCount : '',
          'venue.stats.usersCount' => isset($venue->stats->usersCount) ? $venue->stats->usersCount : '',
          'venue.stats.tipCount' => isset($venue->stats->tipCount) ? $venue->stats->tipCount : '',
          'venue.url' => isset($venue->url) ? $venue->url : '',
          'venue.hereNow.count' => isset($venue->hereNow->count) ? $venue->hereNow->count : '',
          'venue.mayor.user.id' => isset($venue->mayor->user->id) ? $venue->mayor->user->id : '',
          'venue.mayor.user.firstName' => isset($venue->mayor->user->firstName) ? $venue->mayor->user->firstName : '',
          'venue.mayor.user.lastName' => isset($venue->mayor->user->lastName) ? $venue->mayor->user->lastName : '',
          'venue.mayor.user.photo' => isset($venue->mayor->user->photo) ? $venue->mayor->user->photo : '',
          'venue.mayor.user.homeCity' => isset($venue->mayor->user->homeCity) ? $venue->mayor->user->homeCity : '',
          'venue.photos.count' => isset($venue->photos->count) ? $venue->photos->count : '',
          'venue.shortUrl' => isset($venue->shortUrl) ? $venue->shortUrl : '',
          'venue.canonicalUrl' => isset($venue->canonicalUrl) ? $venue->canonicalUrl : '',
        );
        $result->items[] = $item;
      }
    }
    return $result;
  }

  /**
   * Add the extra mapping sources provided by this parser.
   */
  public function getMappingSources() {
    return parent::getMappingSources() + array(
      'venue.id' => array(
        'name' => t('Venue ID'),
        'description' => t('venue.id'),
      ),
      'venue.name' => array(
        'name' => t('Venue name'),
        'description' => t('venue.name'),
      ),
      'venue.contact.formattedPhone' => array(
        'name' => t('Venue phone'),
        'description' => t('venue.contact.formattedPhone'),
      ),
      'venue.contact.twitter' => array(
        'name' => t('Venue Twitter username'),
        'description' => t('venue.contact.twitter'),
      ),
      'venue.location.address' => array(
        'name' => t('Venue location: Street'),
        'description' => t('venue.location.address'),
      ),
      'venue.location.lat' => array(
        'name' => t('Venue location: Latitude'),
        'description' => t('venue.location.lat'),
      ),
      'venue.location.lng' => array(
        'name' => t('Venue location: Longitude'),
        'description' => t('venue.location.lng'),
      ),
      'venue.location.postalCode' => array(
        'name' => t('Venue location: Postal code'),
        'description' => t('venue.location.postalCode'),
      ),
      'venue.location.city' => array(
        'name' => t('Venue location: City'),
        'description' => t('venue.location.city'),
      ),
      'venue.location.state' => array(
        'name' => t('Venue location: State'),
        'description' => t('venue.location.state'),
      ),
      'venue.location.country' => array(
        'name' => t('Venue location: Country'),
        'description' => t('venue.location.country'),
      ),
      'venue.stats.checkinsCount' => array(
        'name' => t('Venue stats: Check-ins count'),
        'description' => t('venue.stats.checkinsCount'),
      ),
      'venue.stats.usersCount' => array(
        'name' => t('Venue stats: Users Count'),
        'description' => t('venue.stats.usersCount'),
      ),
      'venue.stats.tipCount' => array(
        'name' => t('Venue stats: Tips count'),
        'description' => t('venue.stats.tipCount'),
      ),
      'venue.url' => array(
        'name' => t('Venue URL (external)'),
        'description' => t('venue.url'),
      ),
      'venue.hereNow.count' => array(
        'name' => t('Here Now count'),
        'description' => t('venue.hereNow.count'),
      ),
      'venue.mayor.user.id' => array(
        'name' => t('Mayor Foursquare user ID'),
        'description' => t('venue.mayor.user.id'),
      ),
      'venue.mayor.user.firstName' => array(
        'name' => t('Mayor first name'),
        'description' => t('venue.mayor.user.firstName'),
      ),
      'venue.mayor.user.lastName' => array(
        'name' => t('Mayor last name'),
        'description' => t('venue.mayor.user.lastName'),
      ),
      'venue.mayor.user.photo' => array(
        'name' => t('Mayor user photo (URL)'),
        'description' => t('venue.mayor.user.photo'),
      ),
      'venue.mayor.user.homeCity' => array(
        'name' => t('Mayor home city'),
        'description' => t('venue.mayor.user.homeCity'),
      ),
    );
  }

}
