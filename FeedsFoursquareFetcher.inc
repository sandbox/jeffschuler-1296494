<?php

/**
 * @file
 * Feeds fetcher class for Fourquare
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsFileFetcher.
 */
class FeedsFoursquareFetcherResult extends FeedsFetcherResult {
  protected $venue_id;
  protected $client_id;
  protected $client_secret;
  protected $access_token;
  
  /**
   * Constructor.
   */
  public function __construct($venue_id, $config) {
    $this->venue_id = $venue_id;    
    $this->client_id = $config['client_id'];
    $this->client_secret = $config['client_secret'];
    $this->access_token = $config['access_token'];
    parent::__construct('');
  }

  /**
   * Overrides parent::getRaw().
   */
  public function getRaw() {
    require_once DRUPAL_ROOT . '/sites/all/libraries/foursquare-async/EpiCurl.php';
    require_once DRUPAL_ROOT . '/sites/all/libraries/foursquare-async/EpiFoursquare.php';

    $fsObj = new EpiFoursquare($this->client_id, $this->client_secret, $this->access_token);

    $response = $fsObj->get('/venues/' . $this->venue_id);
    $response_json = $response->responseText;

    return $response_json;
  }
}

/**
 * Class definition for Foursquare fetcher.
 *
 * Fetches data from Foursquare API
 */
class FeedsFoursquareFetcher extends FeedsFetcher {

  /**
   * Fetch content from Foursquare API and return it.
   *
   * @param $source FeedsSource
   *
   * @see FeedsFetcher::fetch()
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    //$credentials = array('client_id'=> , 'client_secret'=> , 'access_token'=> , )
    return new FeedsFoursquareFetcherResult($source_config['venue_id'], $this->config);
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['venue_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Venue ID'),
      '#description' => t('Foursquare Venue ID'),
      '#default_value' => isset($source_config['venue_id']) ? $source_config['venue_id'] : '',
    );
    return $form;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();
    $form['client_id'] = array(
      '#type' =>'textfield',
      '#title' => t('Client ID'),
      '#description' => t('Foursquare API Client ID'),
      '#default_value' => $this->config['client_id'],
    );
    $form['client_secret'] = array(
      '#type' =>'textfield',
      '#title' => t('Client Secret'),
      '#description' => t('Foursquare API Client Secret'),
      '#default_value' => $this->config['client_secret'],
    );
    $form['access_token'] = array(
      '#type' =>'textfield',
      '#title' => t('Access Token'),
      '#description' => t('Foursquare API Access Token'),
      '#default_value' => $this->config['access_token'],
    );
    return $form;
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'client_id' => '',
      'client_secret' => '',
      'access_token' => '',
    );
  }

}
